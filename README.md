<img style="float: right" align="right" src="https://gitlab.com/pastel-fablab/orga/raw/master/logo/fablab-logo-transparent-300dpi.png" width="350">

# Projet Exemple

Ce projet est un exemple/template de projet pour les prochaines réalisation.
Il est important de bien documenter chaque projet, car le partage et la collaboration est une valeur primaire du FabLab.

[mettre un schéma d'architecture générale illutrant le projet. Au pire, une photo]

## Fiche projet 
| **Caractéristiques du Projet**  |   |
| --- | --- |
| **Technologies** | Liste des technos (soft & hard) utilisés dans le cadre du projet
| **Coût** | Estimation du coût total du achat du matériel acheté.
| **Temps de réalisation** | Estimation du temps de réalisation pour reproduire à la lettre ce projet.
| **Difficulté** | Estimation de la difficulté du projet.
| **Equipe réalisatrice** | Liste des personnes qui ont contribué au projet (pour contact)
| **Documentation** | https://gitlab.com/pastel-fablab/projets/projet-exemple

## Matériel

- liste du
- matériel et logiciel requis
- avec des référénces / liens

## Description détaillée 

Ici, il est question de décrire, partie par partie, le projet.
On peut se baser sur des site de références (mettre des liens), ou mettre une architecture générale qui sera explicitée dans des sous-README.

En bref c'est plutôt libre :)

Les images et schémas sont les bienvenues

## Liens

- Mettre ici des liens sur des références de réalisation de projets.
- ou des liens sur des sites qui vont plus loin dans le concept.

## License

Ce projet est sous licence Apache 2.0. Voire le fichier [LICENSE](LICENSE.md) pour plus de détails
